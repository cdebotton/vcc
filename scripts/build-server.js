/* @flow */

import express from 'express';
import webpack from 'webpack';
import invariant from 'invariant';
import dev from 'webpack-dev-middleware';
import hot from 'webpack-hot-middleware';

import config from '../webpack.config.dev';

const debug = require('debug')('vimeo-webpack');

const { PORT } = process.env;

invariant(PORT, 'process.env.PORT is undefined.');

const compiler = webpack(config);
const app = express();

app.use(dev(compiler, {
  hot: true,
  publicPath: config.output.publicPath,
  historyApiFallback: true,
  stats: {
    colors: true,
  },
}));

app.use(hot(compiler));

app.listen(PORT, (err) => {
  if (err) {
    debug(err.toString());
    return;
  }
  debug(`Listening on port ${PORT}.`);
});
