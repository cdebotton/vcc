/* @flow */
/* eslint-disable global-require */

module.exports = {
  plugins: [
    require('postcss-import')(),
    require('postcss-responsive-type'),
    require('postcss-nested'),
    require('postcss-cssnext'),
  ],
};
