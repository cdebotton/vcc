/* @flow */

import fetch from 'isomorphic-fetch';

import type { Dispatch, Store } from 'redux';

import {
  makeChannelInfoURL,
  makeChannelVideosURL,
  HTTPError,
} from '../utils';

/**
 Action responsible for updating current search term within application so data can be shared
 across views.
 **/

export const CHANNEL_UPDATE_TERM = 'CHANNEL_UPDATE_TERM';

export const channelUpdateTerm = (term: string) => ({
  type: CHANNEL_UPDATE_TERM,
  term,
});

/**
 Login for requesting a channel based on an unsanitized input.
 **/

export const CHANNEL_FETCH_REQUEST = 'CHANNEL_FETCH_REQUEST';
export const CHANNEL_FETCH_SUCCESS = 'CHANNEL_FETCH_SUCCESS';
export const CHANNEL_FETCH_FAILURE = 'CHANNEL_FETCH_FAILURE';

const channelFetchRequest = (channelId: string) => ({
  type: CHANNEL_FETCH_REQUEST,
  channelId,
});

const channelFetchSuccess = ({ channelId, payload }) => ({
  type: CHANNEL_FETCH_SUCCESS,
  channelId,
  payload,
});

const channelFetchError = ({ channelId, error }) => ({
  type: CHANNEL_FETCH_FAILURE,
  channelId,
  error,
});

export const CHANNEL_UPDATE_VIDEOS = 'CHANNEL_UPDATE_VIDEOS';

const channelUpdateVideos = ({ channelId, payload }) => ({
  type: CHANNEL_UPDATE_VIDEOS,
  channelId,
  payload,
});


type FetchProps = {
  term?: string;
  onSuccess?: () => void,
};

export const channelFetch = ({ term, onSuccess }: FetchProps) =>
  async (dispatch: Dispatch, getState: Store.S) => {
    // Get current search query value.
    let { searchTerm } = getState().channels;

    // Allow for overriding term, used when loading a channel directly rather than searching.
    if (!searchTerm && term) {
      channelUpdateTerm(term);
      searchTerm = term;
    }

    // Dispatch initial request to start loading process.
    dispatch(channelFetchRequest(searchTerm));

    try {
      // Fetch both the channel info and video list.
      const responses = await Promise.all([
        fetch(makeChannelInfoURL(searchTerm)),
        fetch(makeChannelVideosURL(searchTerm)),
      ]);

      // Check both responses to make sure they were successful. Use to detect HTTP error codes.
      responses.forEach((response) => {
        if (!response.ok) {
          throw new HTTPError(response);
        }
      });

      // Map successful responses to JSON.
      const [info, videos] = await Promise.all(responses.map(response => response.json()));

      // Dispatch successful data to channel info.
      dispatch(channelFetchSuccess({
        channelId: searchTerm,
        payload: info,
      }));

      // Dispatch successful data to video list.
      dispatch(channelUpdateVideos({
        channelId: searchTerm,
        payload: videos,
      }));

      // If an onSuccess callback (typically for redirecting) is passed, call that now.
      if (onSuccess) {
        onSuccess();
      }
    } catch (error) {
      // Dispatch error info.
      dispatch(channelFetchError({
        channelId: searchTerm,
        error,
      }));
    }
  };
