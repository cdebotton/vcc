/* @flow */
/* eslint-disable global-require */

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import Router from 'react-router-dom/BrowserRouter';

import configureStore from './store';

const mount = document.querySelector('#app');
const store = configureStore();

const updater = () => {
  const App = require('./components/App').default;

  render(
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>,
    mount,
  );
};

updater();

if (module.hot) {
  // $FlowIgnore: Flow doesn't understand module.hot object.
  module.hot.accept('./components/App', updater);
}
