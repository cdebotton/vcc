/* @flow

/**
 * Remove whitespaces and non-number/letter characters, also convert to lowercase to
 * attempt to match Vimeos URL-encoded channel names.
 **/

export const humanReadableDigits = (n: number | string) => {
  if (n) {
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  return 0;
};

/**
 * Generate API URLs
 **/

export const makeChannelInfoURL = (term: string) => `https://vimeo.com/api/v2/channel/${term}/info.json`;
export const makeChannelVideosURL = (term: string) => `http://vimeo.com/api/v2/channel/${term}/videos.json`;

/**
 * HTTP Error Object
 */

type HTTPErrorProps = {
  status: number;
  statusText: string;
};

export class HTTPError extends Error {
  status: number;
  constructor({ statusText, status }: HTTPErrorProps) {
    super(statusText);
    this.status = status;
  }
}
