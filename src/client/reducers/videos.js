/* @flow */

import { CHANNEL_UPDATE_VIDEOS } from '../actions/channelActions';

const initialState = {
  loading: false,
  data: {},
};

export type Video = {
  channelId: string;
  description: string;
  duration: number;
  embed_privacy: string;
  height: number;
  id: number;
  stats_number_of_comments: number;
  stats_number_of_likes: number;
  stats_number_of_plays: number;
  tags: string;
  thumbnail_large: string;
  thumbnail_medium: string;
  thumbnail_small: string;
  title: string;
  upload_date: string;
  url: string;
  upload_date: string;
  user_id: number;
  user_name: string;
  user_portrait_huge: string;
  user_portrait_large: string;
  user_portrait_medium: string;
  user_portrait_small: string;
  user_url: string;
  width: number;
};

type State = {
  loading: boolean;
  data: {[key: string]: Video};
};

type Action = {
  type: string;
  channelId: string;
  payload: [Video];
};

const videos = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case CHANNEL_UPDATE_VIDEOS: {
      return {
        ...state,
        data: action.payload.reduce((acc, video) => {
          const key = video.id.toString();

          acc[key] = {
            ...acc[key],
            ...video,
            channelId: action.channelId,
          };

          return acc;
        }, state.data),
      };
    }
    default:
      return state;
  }
};

export default videos;
