/* @flow */

import uuid from 'uuid';
import it from 'ava';
import videos from '../videos';

const createMockVideo = () => ({
  channelId: uuid.v4(),
  description: uuid.v4(),
  duration: uuid.v4(),
  embed_privacy: uuid.v4(),
  height: uuid.v4(),
  id: uuid.v4(),
  stats_number_of_comments: uuid.v4(),
  stats_number_of_likes: uuid.v4(),
  stats_number_of_plays: uuid.v4(),
  tags: uuid.v4(),
  thumbnail_large: uuid.v4(),
  thumbnail_medium: uuid.v4(),
  thumbnail_small: uuid.v4(),
  title: uuid.v4(),
  upload_date: uuid.v4(),
  url: uuid.v4(),
  user_id: uuid.v4(),
  user_name: uuid.v4(),
  user_portrait_huge: uuid.v4(),
  user_portrait_large: uuid.v4(),
  user_portrait_medium: uuid.v4(),
  user_portrait_small: uuid.v4(),
  user_url: uuid.v4(),
  width: uuid.v4(),
});

it('update videos appends videos', (t): void => {
  const channelId = uuid.v4();
  const videoOne = createMockVideo();
  const videoTwo = createMockVideo();
  const videoThree = createMockVideo();

  const action = {
    type: 'CHANNEL_UPDATE_VIDEOS',
    channelId,
    payload: [videoOne, videoTwo, videoThree],
  };

  const result = videos(undefined, action);

  t.deepEqual(result.data[videoOne.id], { ...videoOne, channelId });
  t.deepEqual(result.data[videoTwo.id], { ...videoTwo, channelId });
  t.deepEqual(result.data[videoThree.id], { ...videoThree, channelId });
});
