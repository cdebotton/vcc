/* @flow */

import it from 'ava';
import uuid from 'uuid';
import channels, { sanitizeTerm } from '../channels';

it('can update the search term', (t): void => {
  const term = uuid.v4();

  const action = {
    type: 'CHANNEL_UPDATE_TERM',
    term,
  };

  const result = channels(undefined, action);
  t.is(term, result.term);
});

it('creates a sanitized term when the search term is updated', (t): void => {
  const term = uuid.v4();
  const sanitized = sanitizeTerm(term);

  const action = {
    type: 'CHANNEL_UPDATE_TERM',
    term,
  };

  const result = channels(undefined, action);
  t.is(sanitized, result.searchTerm);
});

it('fetch request sets loading to true', (t): void => {
  const action = {
    type: 'CHANNEL_FETCH_REQUEST',
    channelId: uuid.v4(),
  };

  const result = channels(undefined, action);

  t.is(result.loading, true);
  t.is(result.data[action.channelId].loading, true);
});

it('fetch success sets loading to false', (t): void => {
  const action = {
    type: 'CHANNEL_FETCH_SUCCESS',
    channelId: uuid.v4(),
    payload: {},
  };

  const result = channels(undefined, action);

  t.is(result.loading, false);
  t.is(result.data[action.channelId].loading, false);
});

it('fetch success assigns payload to channelId', (t): void => {
  const channelId = uuid.v4();

  const actionOne = {
    type: 'CHANNEL_FETCH_REQUEST',
    channelId,
  };

  let result = channels(undefined, actionOne);

  const actionTwo = {
    type: 'CHANNEL_FETCH_SUCCESS',
    channelId,
    payload: { name: 'baz' },
  };

  result = channels(result, actionTwo);

  t.is(result.data[actionOne.channelId].name, 'baz');
});

it('fetch failure sets loading to false', (t): void => {
  const channelId = uuid.v4();

  const actionOne = {
    type: 'CHANNEL_FETCH_REQUEST',
    channelId,
  };

  let result = channels(undefined, actionOne);

  const actionTwo = {
    type: 'CHANNEL_FETCH_FAILURE',
    channelId: uuid.v4(),
    payload: {},
  };

  result = channels(result, actionTwo);

  t.is(result.loading, false);
  t.is(result.data[actionTwo.channelId].loading, false);
});


it('fetch failure sets an error to the channel root', (t): void => {
  const error = new Error('Foo Error');

  const action = {
    type: 'CHANNEL_FETCH_FAILURE',
    channelId: uuid.v4(),
    error,
  };

  const result = channels(undefined, action);

  t.is(result.error, error);
});
