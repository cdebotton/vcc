/* @flow */

import { combineReducers } from 'redux';

import videos from './videos';
import channels from './channels';

const rootReducer = combineReducers({
  videos,
  channels,
});

export default rootReducer;
