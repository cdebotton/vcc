/* @flow */

import invariant from 'invariant';

import {
  CHANNEL_UPDATE_TERM,
  CHANNEL_FETCH_REQUEST,
  CHANNEL_FETCH_SUCCESS,
  CHANNEL_FETCH_FAILURE,
} from '../actions/channelActions';

import type { HTTPError } from '../utils';

const initialState = {
  term: '',
  searchTerm: '',
  loading: false,
  error: null,
  data: {},
};

type Channel = {
  creator_display_name: string;
  creator_url: string;
  description: string;
  logo: string;
  name: string;
  total_subscribers: number;
  total_videos: number;
  url: string;
  loading: boolean;
  error: ?Error;
};

type State = {
  term: string;
  searchTerm: string;
  loading: boolean;
  error: ?Error;
  data: {[key: string]: Channel};
};

type Action = {
  term?: string;
  channelId?: string;
  payload?: {[key: string]: Channel};
  type: string;
  error?: Error | HTTPError;
};

export const sanitizeTerm = (term: string) => term.toLowerCase()
  .replace(/[^A-z0-9]/, '');

const channels = (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case CHANNEL_UPDATE_TERM:
      invariant(action.term, 'action.term is undefined.');

      return { ...state, term: action.term, searchTerm: sanitizeTerm(action.term) };
    case CHANNEL_FETCH_REQUEST:
      invariant(action.channelId, 'action.channelId is undefined.');

      return {
        ...state,
        loading: true,
        error: null,
        data: {
          ...state.data,
          [action.channelId]: {
            ...state.data[action.channelId],
            loading: true,
          },
        },
      };
    case CHANNEL_FETCH_SUCCESS:
      invariant(action.channelId, 'action.channelId is undefined.');
      invariant(action.payload, 'action.payload is undefined.');

      return {
        ...state,
        loading: false,
        error: null,
        data: {
          ...state.data,
          [action.channelId]: {
            ...state[action.channelId],
            ...action.payload,
            loading: false,
          },
        },
      };
    case CHANNEL_FETCH_FAILURE:
      invariant(action.channelId, 'action.channelId is undefined.');

      return {
        ...state,
        loading: false,
        error: action.error,
        data: {
          ...state.data,
          [action.channelId]: {
            ...state.data[action.channelId],
            loading: false,
          },
        },
      };
    default:
      return state;
  }
};

export default channels;
