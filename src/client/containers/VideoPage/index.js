/* @flow */
/* eslint-disable react/no-danger */

import React, { Component } from 'react';
import { connect } from 'react-redux';

import type { Element } from 'react';

import PageHeader from '../../components/PageHeader';
import UserBadge from '../../components/UserBadge';
import Stats from '../../components/Stats';

import './video-page.css';

type Props = {
  id: number;
  title: string;
  user_name: string;
  user_portrait_medium: string;
  stats_number_of_comments: number;
  stats_number_of_plays: number;
  stats_number_of_likes: number;
  description: string;
  user_url: string;
};

class VideoPage extends Component<void, Props, void> {
  render(): Element<*> {
    return (
      <div className="video-page">
        <PageHeader>{this.props.title}</PageHeader>
        {this.props.id && (
          <div className="video-page_player-container">
            <iframe
              className="video-page_player"
              src={`https://player.vimeo.com/video/${this.props.id}`}
              frameBorder="0"
              allowFullScreen
            />
          </div>
        )}
        <Stats
          className="video-page_stats"
          numberOfLikes={this.props.stats_number_of_likes}
          numberOfComments={this.props.stats_number_of_comments}
          numberOfPlays={this.props.stats_number_of_plays}
        />
        <UserBadge
          large
          className="video-page_user-badge"
          profileUrl={this.props.user_url}
          userName={this.props.user_name}
          thumbnail={this.props.user_portrait_medium}
        />
        <div className="video-page_description" dangerouslySetInnerHTML={{ __html: this.props.description }} />
      </div>
    );
  }
}

const mapStateToProps = (state, { match }) => {
  const { videoId } = match.params;
  const video = state.videos.data[videoId];

  return {
    ...video,
  };
};

export default connect(mapStateToProps)(VideoPage);
