/* @flow */
/* eslint-disable react/no-danger */

import React, { Component } from 'react';
import Route from 'react-router-dom/Route';
import Redirect from 'react-router-dom/Redirect';
import { connect } from 'react-redux';

import type { Dispatch } from 'redux';
import type { Element } from 'react';

import VideoPage from '../../containers/VideoPage';
import GridItem from '../../components/GridItem';
import PageHeader from '../../components/PageHeader';
import Search from '../../components/Search';
import ExternalLink from '../../components/ExternalLink';
import { channelFetch } from '../../actions/channelActions';
import { humanReadableDigits } from '../../utils';

import './channel-page.css';

import type { HTTPError } from '../../utils';
import type { Video } from '../../reducers/videos';

type Props = {
  match: { url: string };
  channelId: string;
  creator_display_name: string;
  creator_url: string;
  description: string;
  logo: string;
  name: string;
  total_subscribers: number;
  total_videos: number;
  url: string;
  loading: boolean;
  videos: [Video];
  error: ?Error;
  dispatch: Dispatch;
  error: ?Error | ?HTTPError;
};


class ChannelPage extends Component<void, Props, void> {
  componentWillMount() {
    if (!this.props.loading) {
      this.props.dispatch(channelFetch({ term: this.props.channelId }));
    }
  }

  render(): Element<*> {
    return (
      <div className="channel-page">
        {this.props.error && <Redirect to="/" />}
        <Search className="channel-page_search" />
        <header className="channel-page_header">
          {this.props.logo && this.props.logo.trim() !== '' && (
            <ExternalLink
              className="channel-page_logo"
              style={{ backgroundImage: `url(${this.props.logo})` }}
              href={this.props.url}
            >
              {this.props.name}
            </ExternalLink>
          )}
          <div className="channel-page_description">
            <PageHeader>
              <ExternalLink href={this.props.url}>{this.props.name}</ExternalLink>
            </PageHeader>
            <h3>
              <span>By:</span>&nbsp;
              <ExternalLink href={this.props.creator_url}>
                {this.props.creator_display_name}
              </ExternalLink>
            </h3>
            <p dangerouslySetInnerHTML={{ __html: this.props.description }} />
            <ul className="channel-page_stats">
              <li className="channel-page_stat">
                <strong>Videos:</strong> {humanReadableDigits(this.props.total_videos)}
              </li>
              <li className="channel-page_stat">
                <strong>Subscribers:</strong> {humanReadableDigits(this.props.total_subscribers)}
              </li>
            </ul>
          </div>
        </header>
        <Route
          exact
          path={this.props.match.url}
          render={() => {
            if (this.props.videos && this.props.videos.length > 0) {
              return (
                <ul className="channel-page_videos">
                  {this.props.videos.map((video: Video) => {
                    const target = `/${this.props.channelId}/${video.id}`;

                    return (
                      <GridItem key={video.id} href={target} {...video} />
                    );
                  })}
                </ul>
              );
            }

            return null;
          }}
        />
        <Route path={`${this.props.match.url}/:videoId`} component={VideoPage} />
      </div>
    );
  }
}

const mapStateToProps = (state, { computedMatch }) => {
  const { channelId } = computedMatch.params;
  const videos = Object.values(state.videos.data)
    .filter(({ channelId: checkId }: Video) => checkId === channelId)
    .sort((a, b) => {
      if (a.upload_date > b.upload_date) {
        return -1;
      }

      return 1;
    });

  return {
    error: state.channels.error,
    channelId,
    videos,
    ...state.channels.data[channelId],
  };
};

export default connect(mapStateToProps)(ChannelPage);
