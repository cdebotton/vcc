/* @flow */

import React from 'react';
import { connect } from 'react-redux';
import type { Element } from 'react';

import ExternalLink from '../../components/ExternalLink';
import PageHeader from '../../components/PageHeader';
import Search from '../../components/Search';

import type { HTTPError } from '../../utils';

import './search-page.css';

/**
  Search page container would ideally search a list of available channels
  with an autocomplete feature, but as this isn't available in the Simple API,
  we're going to conform natural language on entry to the whitespaceless,
  punctuationless, lowercase format that channels seem to be referenced by in the API.

  This would become a class rather than a stateless component, because ideally,
  we will use the componentWillMount lifecycle method to dispatch an action to fetch
  the list of channels.

  TODO: Grab list of channels programatically to serve.
 **/

type Props = {
  error: Error | HTTPError;
};

const SearchPage = ({ error }: Props): Element<*> => (
  <div className="search-page">
    <PageHeader>Go to a Vimeo Channel</PageHeader>
    <Search
      className="search-page_search"
      placeholder="Please enter a channel name"
    />
    {error && (
      <div className="search-page_error">
        <p><strong>Sorry!</strong> There was a problem finding that channel.</p>
        <p>Try checking <ExternalLink href="https://vimeo.com/channels">Vimeo</ExternalLink> for possible channels!</p>
      </div>
    )}
  </div>
);

const mapStateToProps = ({ channels }) => ({
  error: channels.error,
});

export default connect(mapStateToProps)(SearchPage);
