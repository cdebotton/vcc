/* @flow */

import React from 'react';

import type { Element } from 'react';

import './page-header.css';

type Props = {
  children?: string;
};

const PageHeader = ({ children }: Props): Element<*> => (
  <div className="page-header">
    <h1>{children}</h1>
  </div>
);

PageHeader.defaultProps = {
  children: 'Page Header',
};

export default PageHeader;
