/* @flow */

import React from 'react';
import classNames from 'classnames';

import type { Element } from 'react';

import ExternalLink from '../../components/ExternalLink';

import './user-badge.css';

type Props = {
  thumbnail: string;
  userName: string;
  large?: boolean;
  className?: string;
  profileUrl: string;
};

const UserBadge = ({ thumbnail, userName, large, className, profileUrl }: Props): Element<*> => (
  <ExternalLink
    href={profileUrl}
    className={classNames(['user-badge_video-user', className, {
      'user-badge_video-user_large': large,
    }])}
  >
    <span
      className={classNames(['user-badge_video-user-thumbnail', {
        'user-badge_video-user-thumbnail_large': large,
      }])}
      style={{ backgroundImage: `url(${thumbnail})` }}
    />
    {userName}
  </ExternalLink>
);

UserBadge.defaultProps = {
  large: false,
  className: null,
};

export default UserBadge;
