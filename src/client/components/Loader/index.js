/* @flow */

import React from 'react';
import classNames from 'classnames';
import type { Element } from 'react';

import './loader.css';

type Props = {
  className?: string;
};

const Loader = ({ className }: Props): Element<*> => (
  <div className={classNames(['loader', className])}>
    <div className="loader_spinner" />
    <h5 className="loader_text">Loading...</h5>
  </div>
);

Loader.defaultProps = {
  className: null,
};

export default Loader;
