/* @flow */

import React from 'react';
import classNames from 'classnames';

import type { Element } from 'react';

import { humanReadableDigits } from '../../utils';

import './stats.css';

type Props = {
  numberOfLikes: number;
  numberOfPlays: number;
  numberOfComments: number;
  className?: string;
};

const Stats = ({
  numberOfLikes,
  numberOfComments,
  numberOfPlays,
  className,
}: Props): Element<*> => (
  <dl className={classNames(['stats', className])}>
    <dt>Plays:</dt>
    <dd>{humanReadableDigits(numberOfPlays)}</dd>
    <dt>Likes:</dt>
    <dd>{humanReadableDigits(numberOfLikes)}</dd>
    <dt>Comments:</dt>
    <dd>{humanReadableDigits(numberOfComments)}</dd>
  </dl>
);

Stats.defaultProps = {
  className: null,
};

export default Stats;
