/* @flow */

import React from 'react';
import type { Element } from 'react';

type Props = {
  href: string;
  children?: string | Element<*>;
};

const ExternalLink = ({ href, children, ...rest }: Props) => (
  <a
    href={href}
    {...rest}
    target="_blank"
    rel="noopener noreferrer"
  >
    {children}
  </a>
);

ExternalLink.defaultProps = {
  children: null,
};

export default ExternalLink;
