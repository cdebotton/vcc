/* @flow */

import React from 'react';
import withRouter from 'react-router-dom/withRouter';
import { connect } from 'react-redux';
import classNames from 'classnames';

import type { Element } from 'react';
import type { Dispatch } from 'redux';

import './search.css';

import { channelUpdateTerm, channelFetch } from '../../actions/channelActions';

type Props = {
  className: ?string;
  placeholder: ?string;
  term: string;
  channelId: string;
  push: Function;
  dispatch: Dispatch;
};

const Search = withRouter(({
  className,
  placeholder,
  term,
  channelId,
  push,
  dispatch,
}: Props): Element<*> => {
  const handleSubmit = (event) => {
    event.preventDefault();

    dispatch(channelFetch({
      onSuccess: () => push(`/${channelId}`),
    }));
  };

  const handleChange = (event) => {
    event.preventDefault();
    dispatch(channelUpdateTerm(event.currentTarget.value));
  };

  return (
    <form
      className={classNames(['search', className])}
      onSubmit={handleSubmit}
    >
      <input
        className="search_input"
        placeholder={placeholder}
        onChange={handleChange}
        value={term}
      />
      <button className="search_submit" type="submit">Go</button>
    </form>
  );
});

Search.defaultProps = {
  className: null,
  placeholder: 'Search',
};

const mapStateToProps = state => ({
  term: state.channels.term,
  channelId: state.channels.searchTerm,
});

export default connect(mapStateToProps)(Search);
