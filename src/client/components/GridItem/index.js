/* @flow */

import React from 'react';
import Link from 'react-router-dom/Link';
import type { Element } from 'react';
import UserBadge from '../UserBadge';

import Stats from '../Stats';

import './grid-item.css';

type Props = {
  id: number;
  href: string;
  thumbnail_large: string;
  url: string;
  title: string;
  user_name: string;
  user_url: string;
  user_portrait_medium: string;
  stats_number_of_plays: number;
  stats_number_of_likes: number;
  stats_number_of_comments: number;
};

const GridItem = ({
  href,
  thumbnail_large: thumbnailLarge,
  title,
  user_url: profileUrl,
  user_name: userName,
  user_portrait_medium: userPortraitMedium,
  stats_number_of_plays: numberOfPlays,
  stats_number_of_likes: numberOfLikes,
  stats_number_of_comments: numberOfComments,
}: Props): Element<*> => (
  <li className="grid-item">
    <Link
      className="grid-item_thumbnail"
      to={href}
      style={{ backgroundImage: `url(${thumbnailLarge})` }}
    />
    <div className="grid-item_info">
      <h2><Link to={href}>{title}</Link></h2>
      <UserBadge
        profileUrl={profileUrl}
        userName={userName}
        thumbnail={userPortraitMedium}
      />
      <Stats
        numberOfLikes={numberOfLikes}
        numberOfPlays={numberOfPlays}
        numberOfComments={numberOfComments}
      />
    </div>
  </li>
);

export default GridItem;
