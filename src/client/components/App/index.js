/* @flow */

import React from 'react';
import Switch from 'react-router-dom/Switch';
import Route from 'react-router-dom/Route';
import { connect } from 'react-redux';

import './app.css';

import SearchPage from '../../containers/SearchPage';
import ChannelPage from '../../containers/ChannelPage';
import Loader from '../Loader';

type Props = {
  loading: boolean;
};

const App = ({ loading }: Props) => (
  <div className="app">
    <div className="app_ribbon" />
    {loading && <Loader className="app_loader" />}
    <Switch>
      <Route exact path="/" component={SearchPage} />
      <Route path="/:channelId" component={ChannelPage} />
    </Switch>
  </div>
);

const mapStateToProps = ({ channels }) => ({
  loading: channels.loading,
});

export default connect(mapStateToProps)(App);
