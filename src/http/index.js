/* @flow */

import Koa from 'koa';
import invariant from 'invariant';

const debug = require('debug')('vimeo-http');

const app = new Koa();

const { PORT } = process.env;
invariant(PORT, 'process.env.PORT is undefined');

type Context = {
  body: string;
};

app.use((ctx: Context) => {
  ctx.body = `
<!doctype>
<html lang="en">
<head>
  <title>Vimeo Code Challenge</title>
</head>
<body>
  <div id="app"></div>
  <script src="http://localhost:3001/bundle.js"></script>
</body>
</html>`;
});

app.listen(PORT, (err) => {
  if (err) {
    debug(err.toString());
    return;
  }

  debug(`Listening on port ${PORT}.`);
});
