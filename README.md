# Vimeo Code Challenge

## Setup
### Requirements
1. `node@7.4`
2. `yarn@0.19.1`
3. `flow@0.37.4` (optional)

### Setup
1. Clone repository
2. Run `yarn` to install dependencies.
3. Run `npm run dev` to start the development server
4. Go to `http://localhost:3000` in browser.

## About

1. Was the question/problem clear? Did you feel like something was missing or not explained correctly?
  - The only uncertainty seemed to be the parameters about which channel to pull. I just asked if there was a certain channel or if I should allow the user to select a channel. Hearing that I was supposed to allow the user to select a channel, and seeing the parameters available in the `Simple API`, it was pretty simple to deduce how to generate URI parts based on the channel name.

2.  How much time did you spend on each part: understanding, designing, coding, testing?
  - I spent a few minutes understanding the problem, as well as designing what I would move forward with. In the past I've built many JavaScript front ends based on things such as the Vimeo API and Tumblr API, and have built similar interfaces on past projects.
  - I spent probably about four hours total writing the application.
  - My biggest slip up was a change in the `react-router@4` api due to a recent rewrite, specifically, the removal of the `browserHistory` object from the module. This was how I programmatically drove transitions outside of React components. Because of this, I spent more time than usual rethinking my approach, specifically, driving the route between the channel selection view and the channel page view based on a successful query to the Vimeo API.
  - I spent the least amount of time writing tests, as I only wrote tests for my reducers, probably only about 30 minutes.

3. What would you have done differently if you have more time or resources?
  - If I had more time, I would have written tests for my actions and my components. I focused on the reducers because of the importance of the purity of the application state, there were minimal complex interactions, and I was really just focused on rendering out the UI in a logical and clean way.
  - I would have spent more time on animations, between routes, based on state changes, if given more time.
  - I would have implemented a more intelligent caching mechanism.
  - I would have handled a lossy client-side network by implementing retries.

4. Are there any bottlenecks with your solution? if so, what are they and what can you do to fix them/minimize their impact?
 - I took a conservative approach to querying the channel API as I didn't see value in hammering it to drive live updates.
 - I'm not currently doing any UI optimizations for large lists of videos. If I was able to retrieve more than 20 videos at at time, I would have optimized the UI to minimize the weight of grid elements not on the screen.
 - Similarly, videos are stored in a plain object in the reducer. If the list were to grow to a substantial number, I would have used a separate data layer that would allow me to dequeue information from what's in memory when it's not needed.
 - Typically, I would use a tool such a `reslect` to cache computed state changes, so that the dom remains quick to update. With a list of items this small, it wasn't an issue. When I was working on the audio player for `The Moth` where there are 700+ tracks listening to state changes, I applied caching mechanisms more aggressively.

5. How would the system scale for more users/visitors?
  - My work was largely front-end on this example, so the bottle neck would have really come from requests to the API layer. To alleviate this, I would have implemented a smarter caching system.
  - Right now, data is stored in memory in the reducer, I would have leveraged local storage or a in browser database to cache the data so that requests were only made when necessary.

6. How would your solution cope if the API was slow or broke or returned incorrect data?
  - Right now, I'm simply showing a loading state in the lower right corner to signify that the application is making a request, and rendering an error message when the request returns a 404 or malformed data.
  - Ideally, if I had more time, I would have set a number of retries for failed requests, perhaps to a lossy connection on the client side.

7. Anything else you want to share about your solution or the problem?
  - I tried to loosely cover my set of skills in this test while solving the problem that was asked. I didn't use any pre-set build configurations, and set up the application as I typically do when working on projects for clients.
  - I noticed that the implementation of BEM that's used on Vimeo is a bit different from what I've been working with, which is based largely on the official documentation.
  - I used PostCSS + cssnext, but I'm fully comfortable with SASS, Stylus, and LESS.
  - I think I may have answered a lot of what I would have wanted to share by being a bit too verbose in my previous answers, but I really enjoyed building this application as a sample of my skill-set. I hope that it  matches the quality of work and thought processes that you're looking for.
