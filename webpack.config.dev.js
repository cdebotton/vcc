/* @flow */

import { NoEmitOnErrorsPlugin, HotModuleReplacementPlugin, DefinePlugin } from 'webpack';
import invariant from 'invariant';
import path from 'path';

const { PORT } = process.env;

invariant(PORT, 'process.env.PORT undefined.');

export default {
  devtool: 'eval',
  entry: [
    `webpack-hot-middleware/client?path=http://localhost:${PORT}/__webpack_hmr`,
    'react-hot-loader/patch',
    'babel-polyfill',
    path.join(__dirname, 'src', 'client', 'index.js'),
  ],
  output: {
    filename: 'bundle.js',
    publicPath: `http://localhost:${PORT}/`,
    path: path.join(__dirname, 'dist'),
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader',
      },
    ],
  },
  plugins: [
    new HotModuleReplacementPlugin(),
    new NoEmitOnErrorsPlugin(),
    new DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
        DEBUG: JSON.stringify('vimeo*'),
      },
    }),
  ],
};
